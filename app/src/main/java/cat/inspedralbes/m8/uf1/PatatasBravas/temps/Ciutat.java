package cat.inspedralbes.m8.uf1.PatatasBravas.temps;

import java.util.ArrayList;

public class Ciutat {
    String nom;
    ArrayList<Temperatura> temps;

    Ciutat(String nom, ArrayList<Temperatura> temps){
        this.nom=nom;
        this.temps=temps;
    }
}
