package cat.inspedralbes.m8.uf1.PatatasBravas.temps;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import org.json.JSONException;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;


public class MainActivity extends AppCompatActivity {
    OkHttpClient client = new OkHttpClient();
    String json;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    protected void onClickBuscar(View v) throws JSONException {
        EditText ciutatView = (EditText) findViewById(R.id.main_ciutat);
        String ciutat = ciutatView.getText().toString();

        if (/*comprova si està a la bdd*/ciutat.equals("")){
            Log.i("aa","");
        }

        else{
            String json = getJason(ciutat);
            try {
                JSONManager jsonManager = new JSONManager(json);
                jsonManager.parseInfo();
                Ciutat dadesCiutat = jsonManager.getCiutat();

            }catch (JSONException e){
                e.printStackTrace();
            }

        }
    }

    public String getJason(String ciutat) throws JSONException {

        Request request = new Request.Builder()
                .url("http://api.openweathermap.org/data/2.5/forecast?q="+ciutat+"&appid=316787495099c06e20934bf982b5e567")
                .build();

        client.newCall(request).enqueue(new Callback() {

            @Override public void onFailure(Call call, IOException e) {
                e.printStackTrace();
            }

            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override public void onResponse(Call call, Response response) throws IOException {
                try (ResponseBody responseBody = response.body()) {
                    if (!response.isSuccessful()) throw new IOException("Unexpected code " + response);

                    json = response.body().string();
                }
            }
        });
        return json;
    }
}